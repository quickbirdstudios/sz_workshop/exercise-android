package com.quickbirdstudios.mvvmtraining;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.quickbirdstudios.mvvmtraining.login.LoginFragment;
import com.quickbirdstudios.mvvmtraining.translator.TranslatorFragment;

public class MainActivity extends AppCompatActivity implements Router {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment == null)
            showLogin();
    }

    public void showTranslator(){
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, TranslatorFragment.newInstance())
                .commit();
    }

    @Override
    public void showLogin() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, LoginFragment.newInstance())
                .commit();
    }
}
