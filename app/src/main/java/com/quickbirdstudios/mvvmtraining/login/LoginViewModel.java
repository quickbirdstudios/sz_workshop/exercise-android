package com.quickbirdstudios.mvvmtraining.login;

import io.reactivex.Observable;
import io.reactivex.Observer;

/**
 * Created by Malte Bucksch on 07/06/2018.
 */

public interface LoginViewModel {
    // inputs
    /**
     * TODO PART 1: VIEWMODEL INTERFACE --- ! START HERE !
     */
    // TODO 1: define the inputs for the Email-Text and Password-Text

    // outputs
    // TODO 2: define the outputs for outputIsPasswordValid() and outputIsEmailValid()
    // TODO 2: HINT - use the "ValidationUtils" class
    // TODO 3: define the output for outputIsLoginButtonEnabled()
}