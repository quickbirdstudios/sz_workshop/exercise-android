package com.quickbirdstudios.mvvmtraining.service;

import android.util.Log;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Malte Bucksch on 07/06/2018.
 */
public class UserServiceImpl implements UserService {
    private static final String TAG = "UserServiceImpl";

    public Observable<Boolean> logout() {
        // mocked

        Log.d(TAG, "Logged out");

        return Observable.just(true)
                .delay(500, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io());
    }

    public Observable<Boolean> login(String email, String password) {
        // mocked

        Log.d(TAG, "Logged in");

        return Observable.just(true)
                .delay(500, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io());
    }
}
