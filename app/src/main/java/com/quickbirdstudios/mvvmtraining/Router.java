package com.quickbirdstudios.mvvmtraining;

/**
 * Created by Malte Bucksch on 07/06/2018.
 */
public interface Router {
    void showTranslator();
    void showLogin();
}
