package com.quickbirdstudios.mvvmtraining.translator;

/**
 * Created by Malte Bucksch on 07/06/2018.
 */

public final class TranslatorEngine {
    private TranslatorEngine() {
        //no instance
    }

    public static String translateToGerman(String english) {
        return english+" translated";
    }
}
