package com.quickbirdstudios.mvvmtraining.translator;

import com.quickbirdstudios.mvvmtraining.service.UserService;

import io.reactivex.Observable;
import io.reactivex.Observer;

/**
 * Created by Malte Bucksch on 07/06/2018.
 */
public interface TranslatorViewModel {
    void init(UserService userService);

    // --- inputs
    Observer<String> inputEnglishText();
    Observer<Boolean> inputSaveTrigger();
    Observer<Boolean> inputLogoutTrigger();

    // --- outputs
    Observable<String> outputGermanText();
    Observable<Boolean> outputIsSavingAllowed();
    Observable<String> outputSavedGermanTranslation();
    Observable<Boolean> outputLoggedOut();
}