package com.quickbirdstudios.mvvmtraining.translator;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.jakewharton.rxbinding2.view.RxView;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.quickbirdstudios.mvvmtraining.R;
import com.quickbirdstudios.mvvmtraining.Router;
import com.quickbirdstudios.mvvmtraining.service.UserServiceImpl;
import com.trello.rxlifecycle2.components.support.RxFragment;

/**
 * Created by Malte Bucksch on 07/06/2018.
 */
public class TranslatorFragment extends RxFragment {
    private TranslatorViewModel translatorViewModel;
    private Router router;

    public static TranslatorFragment newInstance() {
        return new TranslatorFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        translatorViewModel = ViewModelProviders.of(this).get(TranslatorViewModelImpl.class);
        translatorViewModel.init(new UserServiceImpl());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_translator, container, false);
    }

    @SuppressLint("CheckResult")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Button saveButton = view.findViewById(R.id.saveTextButton);
        Button logoutButton = view.findViewById(R.id.logoutButton);
        TextView germanEditText = view.findViewById(R.id.germanOutputTextView);
        TextView englishTextView = view.findViewById(R.id.englishInputEditText);

        //   *** subscribe to inputs ***
        RxTextView.textChanges(englishTextView)
                .subscribe(text -> translatorViewModel.inputEnglishText().onNext(text.toString()));
        RxView.clicks(saveButton)
                .subscribe(v -> translatorViewModel.inputSaveTrigger().onNext(true));
        RxView.clicks(logoutButton)
                .subscribe(v -> translatorViewModel.inputLogoutTrigger().onNext(true));

        //   *** subscribe to outputs ***
        translatorViewModel.outputGermanText()
                .compose(bindToLifecycle())
                .subscribe(germanEditText::setText);
        translatorViewModel.outputIsSavingAllowed()
                .compose(bindToLifecycle())
                .subscribe(saveButton::setEnabled);
        translatorViewModel.outputSavedGermanTranslation()
                .compose(bindToLifecycle())
                .subscribe(this::saveToClipboard);
        translatorViewModel.outputLoggedOut()
                .compose(bindToLifecycle())
                .subscribe(v -> router.showLogin());
    }

    public void saveToClipboard(String text) {
        Toast.makeText(getActivity(), "Saved to clipboard: " + text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        router = (Router) activity;
    }
}
