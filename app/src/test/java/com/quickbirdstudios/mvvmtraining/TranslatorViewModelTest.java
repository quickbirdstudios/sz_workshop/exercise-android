package com.quickbirdstudios.mvvmtraining;

import com.quickbirdstudios.mvvmtraining.service.UserServiceImpl;
import com.quickbirdstudios.mvvmtraining.translator.TranslatorFragment;
import com.quickbirdstudios.mvvmtraining.translator.TranslatorViewModel;
import com.quickbirdstudios.mvvmtraining.translator.TranslatorViewModelImpl;

import org.junit.Test;

/**
 * Created by Malte Bucksch on 07/06/2018.
 */
public class TranslatorViewModelTest {
    @Test
    public void testTranslation() {
        // given
        TranslatorViewModel translatorViewModel = new TranslatorViewModelImpl();

        // when
        translatorViewModel.inputEnglishText().onNext("Dog");

        // then
        translatorViewModel.outputGermanText().test().assertValue("Hund");
    }
}
