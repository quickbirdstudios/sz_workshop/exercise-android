package com.quickbirdstudios.mvvmtraining;

import com.quickbirdstudios.mvvmtraining.login.LoginViewModel;
import com.quickbirdstudios.mvvmtraining.login.LoginViewModelImpl;

import org.junit.Test;

/**
 * Created by Malte Bucksch on 07/06/2018.
 */
public class LoginViewModelTest {
    LoginViewModel loginViewModel = new LoginViewModelImpl();

    @Test
    public void testEmailValidationSucceeds() {
         // TODO 1: test that the validation succeeds for an arbitrary valid email value (e.g. "test@gmail.com")

    }

    @Test
    public void testEmailValidationFails() {
        // TODO 2: test that the validation fails for an arbitrary invalid email value (e.g. "test")

    }

    @Test
    public void testPasswordValidationSucceeds() {
        // TODO 3: test that the validation succeeds for an arbitrary valid password value (e.g. "test12345")

    }

    @Test
    public void testPasswordValidationFails() {
        // TODO 4: test that the validation fails for an arbitrary invalid password value (e.g. "11" - too short)

    }
}
